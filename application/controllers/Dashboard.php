<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
		$data['title'] = "LAKONTAMU";
		$data['subtitle'] = "Layanan Konsultasi Tatap Muka";
		$data['description'] = "Istem Infromasi Layanan Konsultasi Tatap Muka Badan Pengawasan Keuangan dan Pembangunan";
		$data['view_isi'] = "view_dashboard";
		$this->load->view('layout/template',$data);
	}
}
